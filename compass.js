'use strict';

var determineAngleDeviationFromNorth = require('angle-deviation-from-north');
var angles = require('angles');
var turf = require('@turf/turf');

class Compass
{
    /**
     * @param {HTMLElement} outerCompass 
     * @param {HTMLElement} innerCompass 
     */
    constructor() {
        this.currentOuterCompassRotation = null;
        this.currentInnerCompassRotation = null;
        this.destinationCoordinates = null;
        this.distance = null;
        this.formattedDistance = null;
        /**
         * Must have the form :
         * {
         *   latitude: 0,
         *   longitude: 0
         * }
         */
        this.myPosition = null;
    }

    /**
     * position object must be like :
     * 
     * {
     *  latitude: 0,
     *  longitude: 0
     * }
     * 
     * @param {object} position 
     */
    setCurrentPosition(position) {
        this.myPosition = position;
        this._rotateInnerCompass();
        this._setNewDistance();
    }

    /**
     * @param {int} alpha 
     */
    setCurrentOrientation(alpha) {
        this.currentOuterCompassRotation = alpha;
    }

    /**
     * @param {int} lat 
     * @param {int} lon 
     */
    setDestinationCoordinates(lat, lon) {
        this.destinationCoordinates = {
            latitude: lat,
            longitude: lon
        };
        this._rotateInnerCompass();
        this._setNewDistance();
    }

    _rotateInnerCompass() {
        // then we rotate inner compass only if we have the data for it
        if (this.myPosition === null) {
            return;
        }

        if (this.destinationCoordinates === null) {
            return;
        }

        /** For the inner compass it is useless to determine optimized rotation */
        this.currentInnerCompassRotation = determineAngleDeviationFromNorth(this.myPosition, this.destinationCoordinates);
    }

    /**
     * Resolve optimized rotation so that the compass
     * will not have to go more than 180 degrees at 
     * once
     */
    _resolveOptimizedRotation(currentRotation, alpha) {
        var dir = angles.shortestDirection(alpha, currentRotation);
        var distance = angles.distance(currentRotation, alpha);
        var result = dir * distance + currentRotation;
        return result;
    }

    _setNewDistance() {
        if (this.myPosition === null || this.destinationCoordinates === null) {
            return;
        }

        var myPosition = turf.point([this.myPosition.longitude, this.myPosition.latitude]);
        var destination = turf.point([this.destinationCoordinates.longitude,this.destinationCoordinates.latitude]);
        var options = {units: 'kilometers'};

        this.distanceToDestination = Math.round(turf.distance(myPosition, destination, options) * 1000, 0);
        if (this.distanceHtml !== null) {
            if (this.distanceToDestination > 1000) {
                var display = this.distanceToDestination.toFixed(2) / 1000 + ' km';
            } else {
                var display = this.distanceToDestination + ' m';
            }

            this.formattedDistance = display;
        }
    }

    isReady() {
        return this.currentInnerCompassRotation !== null
            && this.currentOuterCompassRotation !== null;
    }

    getTotalRotation(currentRotation) {
        if (this.isReady()) {
            return this._resolveOptimizedRotation(currentRotation, this.currentInnerCompassRotation - this.currentOuterCompassRotation);
        }

        return null;
    }
}

module.exports = Compass;