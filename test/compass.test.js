'use strict';

var Compass = require('../compass');

test('Test outer compass rotation', () => {
    var compass = new Compass();

    compass.setCurrentOrientation(100);
    expect(compass.currentOuterCompassRotation).toEqual(100);

    compass.setCurrentOrientation(279);
    expect(compass.currentOuterCompassRotation).toEqual(279);

    compass.setCurrentOrientation(10);
    expect(compass.currentOuterCompassRotation).toEqual(10);

    compass.setCurrentOrientation(70);
    expect(compass.currentOuterCompassRotation).toEqual(70);
});

test('Test inner compass direction', () => {
    var compass = new Compass();
    compass.setCurrentPosition({
        latitude: 0,
        longitude: 0
    });

    compass.setDestinationCoordinates(0, 1);
    expect(compass.currentInnerCompassRotation).toEqual(90);

    compass.setDestinationCoordinates(1, 0);
    expect(compass.currentInnerCompassRotation).toEqual(0);
});

test('Test total rotation', () => {
    var compass = new Compass();
    compass.setCurrentPosition({
        latitude: 0,
        longitude: 0
    });

    compass.setDestinationCoordinates(0, 1);
    compass.setCurrentOrientation(45);

    expect(compass.getTotalRotation(180)).toEqual(45);
    expect(compass.getTotalRotation(450)).toEqual(405);

    compass.setCurrentOrientation(350);
    expect(compass.getTotalRotation(0)).toEqual(100);

    compass.setDestinationCoordinates(1, 0);
    compass.setCurrentOrientation(0);
    expect(compass.getTotalRotation(0)).toEqual(0);

    // Voir ce qui se passe si destination = current position
});

test('Test distance calculation', () => {
    var compass = new Compass();

    compass.setDestinationCoordinates(46.587920, 0.330350);
    compass.setCurrentPosition({
        latitude: 48.750670,
        longitude: 2.288520
    });

    expect(compass.distanceToDestination).toEqual(281639);
});